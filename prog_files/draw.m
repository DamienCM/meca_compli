% Create a 3 RRR planar robot

% P, Q and R along a 200 mm circle
P = [100, 0];
Q = [100*cos(2*pi/3), 100*sin(2*pi/3)];
R = [100*cos(4*pi/3), 100*sin(4*pi/3)];

% Plot the points and origin and the circle
hold on;
axis equal;
plot(P(1), P(2), 'ro', 'MarkerSize', 5);
plot(Q(1), Q(2), 'ro', 'MarkerSize', 5);
plot(R(1), R(2), 'ro', 'MarkerSize', 5);
plot(0, 0, 'bo', 'MarkerSize', 5);


% Define the firsts legs point
a = 20; %leg length
b = 30;
thetas = [0, 0, 0];

D = [a*cos(thetas(1)) a*sin(thetas(1))];
E = [a*cos(thetas(2)) a*sin(thetas(2))];
F = [a*cos(thetas(3)) a*sin(thetas(3))];

% Plot the segments P-D, Q-E, R-F