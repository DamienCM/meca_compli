from numpy.linalg import norm
from Robot import Robot
import numpy as np
import matplotlib.pyplot as plt
from tikzplotlib import save

if __name__ == '__main__':
    # Initialisation des figures

    # --- Define Robot Parameters ---
    N = 20  # Arrays number of points
    L = 150  # Radius max to be tested
    PHI = 0  # Angle of the effector
    R = np.linspace(0, L, N)  # Radius vector
    A = np.linspace(-np.pi/3, np.pi/3, N)  # Angle vector

    rr,aa = np.meshgrid(R, A)  # grille polaire (radius, angle)

    # --- Create the robot ---
    rrr = Robot()
    fig, ax = plt.subplots(figsize=(16, 16))
    ax.axis('equal')
    print("Robot created")

    # --- Joints without boundaries ---
    thetap1 = []  # Collection of thetas in pose 1
    thetap2 = []  # Collection of thetas in pose 2
    reachable = []
    non_reachable = []
    i=0
    # For all the plan points
    for ri, ai in np.ndindex(rr.shape):
        i+=1
        # Get the x, y coordinate of a r,a point
        x, y = rr[ri][ai]*np.cos(aa[ri][ai]), rr[ri][ai]*np.sin(aa[ri][ai])
        # Try to solve the MGI
        try:
            thetas, _ = rrr.invert_geometric_model(x, y, PHI)

            # (theta1, theta2, theta3) for this point
            t1 = [thetas[i][0] for i in range(3)]  # angles for pose 1
            t2 = [thetas[i][1] for i in range(3)]  # angles for pose 2

            # Add to the colection
            thetap1.append(t1)
            thetap2.append(t2)

            # plot the point in green if it can be solved
            # ax.scatter(x, y, color='green')
            reachable.append((x,y))
        except ValueError as e:
            # pass
            # If the model cannot be solved plot point in red
            # ax.scatter(x, y, color='red')
            non_reachable.append((x,y))
    ax.scatter([x for x,y in reachable], [y for x,y in reachable], color='green')
    ax.scatter([x for x,y in non_reachable], [y for x,y in non_reachable], color='red')
    save('espace_sans_restriction.tex')
    plt.show()

    # --- With joints limits ---
    # Define the matplotlib figure
    fig, ax = plt.subplots(figsize=(16, 16))
    ax.axis('equal')
    # New constant for the max angle in each joint
    ANGLE_MAX = 5*np.pi/180

    # Define x, y, phi = 0, 0, 0 as initial position
    # Angles will be computed from this position
    # Use only pose 1 for following code
    thetas_init, psis_init = rrr.invert_geometric_model(
        0, 0, 0)
    # Extract pose 1
    thetas_init_p1 = [thetas_init[i][0] for i in range(3)]
    psis_init_p1 = [psis_init[i][0] for i in range(3)]

    # Pre plot the base
    # rrr.plot_base()

    # Joints Angles collections
    thetap1 = []
    psip1 = []

    # For each point
    for i in range(len(R)):
        for j in range(len(A)):
            # Compute x,y coordinates
            x, y = R[i]*np.cos(A[j]), R[i]*np.sin(A[j])
            try:
                thetas, psis = rrr.invert_geometric_model(
                    x, y, PHI)

                # Substract initial angle to compute the current angle in the joint
                t1 = [thetas[i][0]-thetas_init_p1[i] for i in range(3)]
                p1 = [psis[i][0]-psis_init_p1[i] for i in range(3)]

                # Add to the collection

                # Unpack for each joint to compute if it has not exceeded the limit
                ta, tb, tc = t1
                pa, pb, pc = p1

                # If the position is reachable with joint limit
                if (abs(ta) < ANGLE_MAX and abs(tb) < ANGLE_MAX and abs(tc) < ANGLE_MAX) and (abs(pa) < ANGLE_MAX and abs(pb) < ANGLE_MAX and abs(pc) < ANGLE_MAX):
                    ax.scatter(x, y, color='green')
                    thetap1.append(t1)
                    psip1.append(p1)
                # If the position is not reachable with joint limit
                else:
                    ax.scatter(x, y, color='purple')

            # If the position is not reachable at all
            except ValueError as e:
                # pass
                ax.scatter(x, y, color='red')
    save('espace_restreint.tex')
    # TODO Faire les calculs de debattements sur les ta tb tc (prendre directmeent thetap1)
    fig.show()

    thetap1 = np.array(thetap1)
    psip1 = np.array(psip1)

    for i in range(3):
        print(f"Jambe {i} :")
        t = thetap1[:, i]
        debattement_actif = np.max(t)-np.min(t)
        p = psip1[:, i]
        debattement_passif = np.max(p)-np.min(p)
        # Check if correct debattement < 2*ANGLE_MAX
        # print(f'{ANGLE_MAX=}')
        # print(f'{np.max(t)=}')
        # print(f'{np.max(p)=}')
        # print(f'{np.min(t)=}')
        # print(f'{np.min(p)=}')
        print(f'{debattement_actif=} rad ~ {debattement_actif*180/np.pi}deg')
        print(f'{debattement_passif=} rad ~ {debattement_passif*180/np.pi}deg')

# Plots them in extreme conditions ...
