from numpy.linalg import norm
from Robot import Robot
import numpy as np
import matplotlib.pyplot as plt
from tikzplotlib import save

if __name__ == '__main__':
    # Initialisation des figures

    # --- Define Robot Parameters ---
    N = 500  # Arrays number of points
    L = 150  # Radius max to be tested
    PHI = 0  # Angle of the effector
    X = np.linspace(-L, L, N)  # length vector
    Y = np.linspace(-L, L, N)  #  vector

    xx,yy = np.meshgrid(X, Y)  # grille

    # --- Create the robot ---
    rrr = Robot()
    fig, ax = plt.subplots(figsize=(16, 16))
    ax.axis('equal')
    print("Robot created")

    # --- Joints without boundaries ---
        # New constant for the max angle in each joint
    ANGLE_MAX = 5*np.pi/180

    # Define x, y, phi = 0, 0, 0 as initial position
    # Angles will be computed from this position
    # Use only pose 1 for following code
    thetas_init, psis_init = rrr.invert_geometric_model(
        0, 0, 0)
    # Extract pose 1
    thetas_init_p1 = [thetas_init[i][0] for i in range(3)]
    psis_init_p1 = [psis_init[i][0] for i in range(3)]

    # Pre plot the base
    # rrr.plot_base()

    # Joints Angles collections
    thetap1 = []
    psip1 = []
    thetap1 = []  # Collection of thetas in pose 1
    thetap2 = []  # Collection of thetas in pose 2
    # For all the plan points
    reacheable = np.zeros((N,N))
    for xi, yi in np.ndindex(xx.shape):
        # print(xi)
        # Get the x, y coordinate of a r,a point
        # x, y = xx[xi][yi]*np.cos(yy[xi][yi]), xx[xi][yi]*np.sin(yy[xi][yi])
        # Try to solve the MGI
        x,y = xx[xi][yi], yy[xi][yi]
        try:
            thetas, psis = rrr.invert_geometric_model(
                x, y, PHI)

            # Substract initial angle to compute the current angle in the joint
            t1 = [thetas[i][0]-thetas_init_p1[i] for i in range(3)]
            p1 = [psis[i][0]-psis_init_p1[i] for i in range(3)]

            # Add to the collection

            # Unpack for each joint to compute if it has not exceeded the limit
            ta, tb, tc = t1
            pa, pb, pc = p1

            # If the position is reachable with joint limit
            if (abs(ta) < ANGLE_MAX and abs(tb) < ANGLE_MAX and abs(tc) < ANGLE_MAX) and (abs(pa) < ANGLE_MAX and abs(pb) < ANGLE_MAX and abs(pc) < ANGLE_MAX):
                # ax.scatter(x, y, color='green')
                reacheable[xi][yi] = 1
                thetap1.append(t1)
                psip1.append(p1)
            # If the position is not reachable with joint limit
            else:
                # ax.scatter(x, y, color='purple')
                reacheable[xi][yi] = 1

            # If the position is not reachable at all
        except ValueError as e:
            # pass
            # ax.scatter(x, y, color='red')
            reacheable[xi][yi] = 0

    plt.imshow(reacheable, cmap='gray', interpolation='none', extent=[-L, L, -L, L], origin='lower')
    # save('espace_sans_restriction.tex')
    plt.xlabel('x (mm)')
    plt.ylabel('y (mm)')

    plt.savefig('espace_sans_restriction.png', dpi=600)
    plt.show()
      # --- With joints limits ---
    # Define the matplotlib figure
    # fig, ax = plt.subplots(figsize=(16, 16))
    # ax.axis('equal')
    # # New constant for the max angle in each joint
    # ANGLE_MAX = 5*np.pi/180

    # # Define x, y, phi = 0, 0, 0 as initial position
    # # Angles will be computed from this position
    # # Use only pose 1 for following code
    # thetas_init, psis_init = rrr.invert_geometric_model(
    #     0, 0, 0)
    # # Extract pose 1
    # thetas_init_p1 = [thetas_init[i][0] for i in range(3)]
    # psis_init_p1 = [psis_init[i][0] for i in range(3)]

    # # Pre plot the base
    # # rrr.plot_base()

    # # Joints Angles collections
    # thetap1 = []
    # psip1 = []

#     # For each point
#     for i in range(len(R)):
#         for j in range(len(A)):
#             # Compute x,y coordinates
#             x, y = R[i]*np.cos(A[j]), R[i]*np.sin(A[j])
#             try:
#                 thetas, psis = rrr.invert_geometric_model(
#                     x, y, PHI)

#                 # Substract initial angle to compute the current angle in the joint
#                 t1 = [thetas[i][0]-thetas_init_p1[i] for i in range(3)]
#                 p1 = [psis[i][0]-psis_init_p1[i] for i in range(3)]

#                 # Add to the collection

#                 # Unpack for each joint to compute if it has not exceeded the limit
#                 ta, tb, tc = t1
#                 pa, pb, pc = p1

#                 # If the position is reachable with joint limit
#                 if (abs(ta) < ANGLE_MAX and abs(tb) < ANGLE_MAX and abs(tc) < ANGLE_MAX) and (abs(pa) < ANGLE_MAX and abs(pb) < ANGLE_MAX and abs(pc) < ANGLE_MAX):
#                     ax.scatter(x, y, color='green')
#                     thetap1.append(t1)
#                     psip1.append(p1)
#                 # If the position is not reachable with joint limit
#                 else:
#                     ax.scatter(x, y, color='purple')

#             # If the position is not reachable at all
#             except ValueError as e:
#                 # pass
#                 ax.scatter(x, y, color='red')
#     save('espace_restreint.tex')
#     # TODO Faire les calculs de debattements sur les ta tb tc (prendre directmeent thetap1)
#     fig.show()

#     thetap1 = np.array(thetap1)
#     psip1 = np.array(psip1)

#     for i in range(3):
#         print(f"Jambe {i} :")
#         t = thetap1[:, i]
#         debattement_actif = np.max(t)-np.min(t)
#         p = psip1[:, i]
#         debattement_passif = np.max(p)-np.min(p)
#         # Check if correct debattement < 2*ANGLE_MAX
#         # print(f'{ANGLE_MAX=}')
#         # print(f'{np.max(t)=}')
#         # print(f'{np.max(p)=}')
#         # print(f'{np.min(t)=}')
#         # print(f'{np.min(p)=}')
#         print(f'{debattement_actif=} rad ~ {debattement_actif*180/np.pi}deg')
#         print(f'{debattement_passif=} rad ~ {debattement_passif*180/np.pi}deg')

# # Plots them in extreme conditions ...
