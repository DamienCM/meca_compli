import numpy as np
from numpy import pi

A = 3
B = 4
C = 2



def solve_angle(A,B,C):
    # inspired by : https://www.youtube.com/watch?v=2INX2YMWiKc
    
    # solving : #  Acosx - Bsinx = C 

    # solution 1
    alpha = np.arctan(B/A)
    R = np.sqrt(A**2 + B**2)
    theta1 = np.arccos(C/R)-alpha
    print("theta1 = ", theta1*180/pi)
    
    # solution 2
    theta2 = -np.arccos(C/R)-alpha
    print("theta2 = ", theta2*180/pi)

    # verif solution 1
    print(f"{A*np.cos(theta1)-B*np.sin(theta1)} =? {C}")
    # verif solution 2
    print(f"{A*np.cos(theta2)-B*np.sin(theta2)} =? {C}")



theta(A,B,C)