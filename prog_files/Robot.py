from cProfile import label
import numpy as np
from numpy import cos, invert, sin, pi
import matplotlib.pyplot as plt
from numpy.core.numeric import ones_like
from numpy.linalg import norm
from numpy import nan, pi, zeros
from scipy.optimize import minimize
import tikzplotlib
from tqdm import tqdm
from tikzplotlib import save as tikz_save


plt.rc('text', usetex=True)
"""
This program file is made to plot and simulate a RRR planar robot. 

Function : 
- plot_RRR_robot(x,y,theta,colors) : plots the robot in the given position and orientation with invert kinematics and geometric model
- plot_leg(n,thetas, colors) : plots the leg in the given position 
- plot_base(xs, ys , color) : plots the base in the given position 
"""


def solve_angle(A, B, C):
    # inspired by : https://www.youtube.com/watch?v=2INX2YMWiKc

    # solving : #  Acosx - Bsinx = C

    # solution 1        alpha = np.linspace(0, 2*pi, 300)
    alpha = np.arctan2(B, A)
    R = np.sqrt(A**2 + B**2)
    theta1 = np.arccos(C/R)-alpha
    # solution 2
    theta2 = -np.arccos(C/R)-alpha
    if np.isnan(theta1) or np.isnan(theta2):
        raise ValueError("Position non atteignable")
    return (theta1, theta2)


class Robot:

    def __init__(self, **kwargs):
        """
            Create robot from given parameters (if no values input, using default)
        """
        self.params = {
            # Constants defining the robot geometry
            # Base
            'BASE_RADIUS': [150, 150, 150],  # Radius of the base circle,
            # angles of the base dots,
            'BASE_ANGLES': np.arange(0, 2*pi, 2*pi/3),
            # angle entre les sommets de l'effecteur
            'EFFECTOR_ANGLES': np.arange(0, 2*pi, 2*pi/3),
            'EFFECTOR_RADIUS': [15, 15, 15],
            'LENGTHS':  # length of leg1, leg2, leg3
            [[100, 100],
             [100, 100],
             [100, 100]],
            # Define the initial position
            'x': 0,
            'y': 0,
            'phi': 0,
            'young_modulus': 210e9,
            'elastic_limit': 1e9,
            'stiffness': [
                [5.9, 5.9, 5.9],  # Leg 1 joints stifness coeff from base to effector
                [5.9, 5.9, 5.9],  # Leg 2 joints
                [5.9, 5.9, 5.9]  # Leg 3
            ]
        }

        # Update robot with the current arguments
        self.params.update(kwargs)
        self.BASE_RADIUS = self.params['BASE_RADIUS']
        self.BASE_ANGLES = self.params['BASE_ANGLES']
        self.EFFECTOR_ANGLES = self.params['EFFECTOR_ANGLES']
        self.EFFECTOR_RADIUS = self.params['EFFECTOR_RADIUS']
        self.LENGTHS = self.params['LENGTHS']
        self.x = self.params['x']
        self.y = self.params['y']
        self.phi = self.params['phi']
        self.young_modulus = self.params['young_modulus']
        self.elastic_limit = self.params['elastic_limit']
        self.stiffness = self.params['stiffness']
        # Joint angles not computed yet
        self.thetas = None
        self.psis = None

        # Compute the thetas and psis
        self.thetas, self.psis = self.invert_geometric_model()

        # Define the x0 vector for direct kinematics
        self.x0 = [
            self.x,
            self.y,
            self.phi,
            self.psis[0][0],
            self.psis[1][0],
            self.psis[2][0]
        ]

        # Define the matplotlib figure for our robot
        # self.fig, self.ax = plt.subplots(figsize=(10, 10))
        # self.ax.axis('equal')
        # self.ax.set_xlim(-350, 350)
        # self.ax.set_ylim(-350, 350)

        # # Define the lines for plotting the robot
        # self.hl1p1, = self.ax.plot([], [], color='purple', ls="-",
        #                            marker='o', mfc='black', mec='black')
        # self.hl2p1, = self.ax.plot([], [], color='purple', ls="-",
        #                            marker='o', mfc='black', mec='black')
        # self.hl3p1, = self.ax.plot([], [], color='purple', ls="-",
                                #    marker='o', mfc='black', mec='black')

        # pose 2
        # lines
        # self.hl1p2, = self.ax.plot([], [], color='blue', ls="-",
        #                            marker='o', mfc='black', mec='black', zorder=2)
        # self.hl2p2, = self.ax.plot([], [], color='blue', ls="-",
        #                            marker='o', mfc='black', mec='black', zorder=2)
        # self.hl3p2, = self.ax.plot([], [], color='blue', ls="-",
        #                            marker='o', mfc='black', mec='black', zorder=2)

        # Init triangle later
        # self.triangle = None

        # # TEMPORARY
        # self.beta1 = 0
        # self.beta2 = 0
        # self.beta3 = 0
        # self.bl1 = self.ax.plot([], [], color='yellow', ls="-",
        #                         marker='o', mfc='black', mec='black', zorder=2)
        # self.bl2 = self.ax.plot([], [], color='yellow', ls="-",
        #                         marker='o', mfc='black', mec='black', zorder=2)
        # self.bl3 = self.ax.plot([], [], color='yellow', ls="-",
        #                         marker='o', mfc='black', mec='black', zorder=2)

    def plot_base(self, color='red'):
        """
        plots the base circle and dots with the input angles and radius
        """
        for r, theta, ls in zip(self.BASE_RADIUS, self.BASE_ANGLES, self.LENGTHS):
            # Plots the base dots
            xs = np.cos(theta) * r
            ys = np.sin(theta) * r
            self.ax.scatter(xs, ys, color=color, marker='o', s=100, zorder=0)
            # Plots the base circle and legs circles
            self.ax.plot([0, np.cos(theta) * r],
                         [0, np.sin(theta) * r], color=color, zorder=0)
            # Draw circles around the dots
            alpha = np.linspace(0, 2*pi, 100)
            # ax.plot(sum(ls)*np.cos(alpha)+xs, sum(ls)*np.sin(alpha)+ys, color=COLOR)

    def plot_legs(self, pose=0, leg_color='blue'):
        """
        plots the legs and the base in the given position and joint them with the effector
        pose 0 : plots both 
        pose 1 : plot 1 (main pose)
        pose 2 : plot pose 2 (alternative pose)
        """
        anglesp1 = None
        anglesp2 = None
        if pose == 1 or pose == 0:
            anglesp1 = [(self.thetas[0][0], self.psis[0][0]), (self.thetas[1][0], self.psis[1][0]), (
                self.thetas[2][0], self.psis[2][0])]
        if pose == 2 or pose == 0:
            anglesp2 = [(self.thetas[0][1], self.psis[0][1]), (self.thetas[1][1], self.psis[1][1]), (
                self.thetas[2][1], self.psis[2][1])]

        for [anglesp, [l1, l2, l3], [bl1, bl2, bl3]] in zip([anglesp1, anglesp2], [(self.hl1p1, self.hl2p1, self.hl3p1), (self.hl1p2, self.hl2p2, self.hl3p2)], [(self.bl1, self.bl2, self.bl3)]):
            if anglesp is not None:  # If valid pose
                leg_poses = []
                # For each leg
                for theta, base_angle, base_radius, length, beta in zip(anglesp, self.BASE_ANGLES, self.BASE_RADIUS, self.LENGTHS, [self.beta1, self.beta2, self.beta3]):
                    # Plot the first leg (starting from the base)
                    x0, y0 = np.cos(base_angle) * \
                        base_radius, np.sin(base_angle) * base_radius
                    # Add the angle and base length to the base position
                    x1, y1 = x0 + np.cos(theta[0]) * length[0], y0 + \
                        np.sin(theta[0]) * length[0]
                    # Plot the line
                    # Add a black dot for the joint 1
                    # ax.scatter([x1], [y1], color='black', marker='o', s=30, zorder=10)

                    # Plot the second leg (starting from the first leg)
                    # First compute the angle of the second leg
                    theta_2 = theta[0] + theta[1]
                    # Add the angle and base length to the first leg position
                    x2, y2 = x1 + np.cos(theta_2) * \
                        length[1], y1 + np.sin(theta_2) * length[1]

                    # Testing
                    x3, y3 = x2+15*np.cos(theta[0]+theta[1]+beta), y2 + \
                        15*np.sin(theta[0]+theta[1]+beta)
                    # Plot the line
                    # Add a black dot for the joint 2
                    # ax.scatter([x2], [y2], color='black', marker='o', s=30, zorder=10)
                    leg_poses.append(([x0, x1, x2, x3], [y0, y1, y2, y3]))

                l1.set_data(leg_poses[0])
                l2.set_data(leg_poses[1])
                l3.set_data(leg_poses[2])
        return leg_poses

    def plot_effector_inverse(self, color='green'):
        """
        This function plots the effector in the given pose
        """
        P = [
            (self.x + self.EFFECTOR_RADIUS[0]*np.cos(self.EFFECTOR_ANGLES[0]+self.phi),
             self.y + self.EFFECTOR_RADIUS[0]*np.sin(self.EFFECTOR_ANGLES[0]+self.phi)),
            (self.x + self.EFFECTOR_RADIUS[1]*np.cos(self.EFFECTOR_ANGLES[1]+self.phi),
             self.y + self.EFFECTOR_RADIUS[1]*np.sin(self.EFFECTOR_ANGLES[1]+self.phi)),
            (self.x + self.EFFECTOR_RADIUS[2]*np.cos(self.EFFECTOR_ANGLES[2]+self.phi),
             self.y + self.EFFECTOR_RADIUS[2]*np.sin(self.EFFECTOR_ANGLES[2]+self.phi))
        ]
        if self.triangle is None:
            self.triangle = plt.Polygon(P, closed=True, color=color, zorder=1)
            self.ax.add_patch(self.triangle)
        else:
            self.triangle.set_xy(P)

    def invert_geometric_model(self, *args):
        """
        This function computes the angles of the legs of the robot for a pose x, y, theta

        """
        if len(args) == 3:
            xd = args[0]
            yd = args[1]
            phid = args[2]
        elif len(args) == 0:
            xd = self.x
            yd = self.y
            phid = self.phi
        else:
            raise ValueError("Wrong number of arguments")

        solutions = []
        psis = []
        xais = [np.cos(ang) * r for ang,
                r in zip(self.BASE_ANGLES, self.BASE_RADIUS)]
        yais = [np.sin(ang) * r for ang,
                r in zip(self.BASE_ANGLES, self.BASE_RADIUS)]
        for i in range(3):  # for each leg
            L = self.EFFECTOR_RADIUS[i]
            ai = self.LENGTHS[i][0]
            bi = self.LENGTHS[i][1]
            xai = xais[i]
            yai = yais[i]
            alphai = self.EFFECTOR_ANGLES[i]
            Ci = xd**2 + yd**2 + xai**2 + yai**2 + L**2 + ai**2 - bi**2
            Ci += 2*xd*(-xai+L*np.cos(alphai+phid))
            Ci -= 2*xai*L*np.cos(alphai+phid)
            Ci += 2*yd*(-yai+L*np.sin(alphai+phid))
            Ci -= 2*yai*L*np.sin(alphai+phid)
            Ai = -2*ai*(xd-xai+L*np.cos(alphai+phid))
            Bi = -2*ai*(yd-yai+L*np.sin(alphai+phid))
            # Add solutions to the list
            try:
                theta1, theta2 = solve_angle(Ai, -Bi, -Ci)
            except ValueError as e:
                raise e
            Mi1 = [xai+ai*np.cos(theta1), yai+ai*np.sin(theta1)]
            Mi2 = [xai+ai*np.cos(theta2), yai+ai*np.sin(theta2)]
            Bi = [xd + L*np.cos(alphai+phid), yd + L*np.sin(alphai+phid)]
            solutions.append([theta1, theta2])
            # Computes the psi angle
            vect1 = -np.array(Mi1) + np.array(Bi)
            vect2 = -np.array(Mi2) + np.array(Bi)
            psi1 = np.arctan2(vect1[1], vect1[0])-theta1
            psi2 = np.arctan2(vect2[1], vect2[0])-theta2
            psis.append((psi1, psi2))
            # print(f'jambe {i} : \n \t- actif : {theta1,theta2} \n \t -passif : {psi1,psi2} ')
        # Return solutions for each leg
        self.thetas = solutions
        self.psis = psis
        return solutions, psis

    def direct_kinematics(self, thetas=None, x0=None):
        """
        This function computes the angles of the legs of the robot for a pose theta1, thetha2, theta3
        input : None
                or specified :
                                thetas = angle of the legs [[th0],[th1],[th2]] format
                                x0 = initial value of the solution for the solver
        return x, y, phi, psi1, psi2, psi3
        """
        if thetas is not None:
            if np.shape(thetas) != (3, 0):
                raise ValueError(
                    "The shape of thetas must be (3,0) [[th1],[th2],[th3]]")
            self.thetas = thetas
        if x0 is not None:
            self.x0 = x0

        def vectors(variables):
            """
            Function returning the quadratic-sum error of the 6 equations
            input : [x,y,phi,psi1,psi2,psi3]
            """
            # --- Unpack the values ---
            x = variables[0]
            y = variables[1]
            phi = variables[2]
            psi1 = variables[3] % (2*pi)
            psi2 = variables[4] % (2*pi)
            psi3 = variables[5] % (2*pi)
            psis = [psi1, psi2, psi3]
            zeros = []  # list of the supposed zero angles

            # --- For each leg ---
            for i in range(3):
                # Compute the null vector for x
                vector_x = self.BASE_RADIUS[i]*cos(self.BASE_ANGLES[i])
                vector_x += self.LENGTHS[i][0] * cos(self.thetas[i][0])
                vector_x += self.LENGTHS[i][1] * cos(psis[i]+self.thetas[i][0])
                vector_x -= self.EFFECTOR_RADIUS[i] * \
                    cos(self.EFFECTOR_ANGLES[i] + phi)
                vector_x -= x
                # Compute the null vector for y
                vector_y = self.BASE_RADIUS[i]*sin(self.BASE_ANGLES[i])
                vector_y += self.LENGTHS[i][0] * sin(self.thetas[i][0])
                vector_y += self.LENGTHS[i][1] * sin(psis[i]+self.thetas[i][0])
                vector_y -= self.EFFECTOR_RADIUS[i] * \
                    sin(self.EFFECTOR_ANGLES[i] + phi)
                vector_y -= y
                # append to the "zeros" list
                zeros.append(vector_x)
                zeros.append(vector_y)
            # Return the sum of the quadratic-sum of the 6 equations ie zeros quadratic-sum
            return np.sqrt(sum([z**2 for z in zeros]))

        # Initial angles
        res = minimize(vectors, self.x0)
        if res.fun > 1e-3:
            print("Cannot solve error in direct kinematics")
            raise ValueError("Error in direct_kinematics")

        return res.x  # x, y, phi, psi1, psi2, psi3

    def equivalent_stiffness(self, thetas1: np.ndarray, theta2=None, theta3=None):
        """This function compute the force required to push the mecanism from leg1 (index0)

        Args:
            thetas (np.ndarray): [array of the angle variation]

        Returns:
            [np.array]: [array of the force required at each point]
        """
        # Shortcut if user input
        if theta2 is None:
            theta2 = self.thetas[1][0]
        else :
            theta2 = theta2 + self.thetas[1][0]

        if theta3 is None:
            theta3 = self.thetas[2][0]
        else :
            theta3 = theta3 + self.thetas[2][0]

        # --- Compute the initial value ---
        # get the middle angle for leg 1
        theta10 = thetas1[int(len(thetas1)/2)]
        self.thetas = [[theta10], [theta2], [theta3]]
        # Compute the default angle to get the variatio5n from it
        x0, y0, phi0, psi10, psi20, psi30 = self.direct_kinematics()

        # --- First let's create the vector containing the angles of the legs ---
        psis1 = np.zeros_like(thetas1)
        psis2 = np.zeros_like(thetas1)
        psis3 = np.zeros_like(thetas1)
        # Introducing betas (angle between leg2 and the platform)
        # with beta = -theta - psi + alpha
        betas1 = np.zeros_like(thetas1)
        betas2 = np.zeros_like(thetas1)
        betas3 = np.zeros_like(thetas1)
        # ! Le pi rajoute est parce que l'angle est defini dans le sens inverse (du centre vers l'extremite) et ici on veut de l'extremite vers de le centre
        betas10 = -(theta10+psi10)+self.EFFECTOR_ANGLES[0]+phi0+np.pi
        betas20 = -(theta2+psi20)+self.EFFECTOR_ANGLES[1]+phi0+np.pi
        betas30 = -(theta3+psi30)+self.EFFECTOR_ANGLES[2]+phi0+np.pi

        dx, dy = np.zeros_like(thetas1), np.zeros_like(thetas1) 

        # --- Compute for each single thetas1 angle value the other joints values ---
        for i, th in tqdm(enumerate(thetas1)):
            self.thetas[0][0] = th
            x, y, phi, psi1, psi2, psi3 = self.direct_kinematics()
            psis1[i] = psi1
            psis2[i] = psi2
            psis3[i] = psi3
            betas1[i] = -(th+psi1)+(self.EFFECTOR_ANGLES[0]+phi)+np.pi
            betas2[i] = -(theta2+psi2)+(self.EFFECTOR_ANGLES[1]+phi)+np.pi
            betas3[i] = -(theta3+psi3)+(self.EFFECTOR_ANGLES[2]+phi)+np.pi
            dx[i] = x-x0
            dy[i] = y-y0

            # self.beta1 = betas1[i]
            # self.beta2 = betas2[i]
            # self.beta3 = betas3[i]
            # self.x = x
            # self.y = y
            # self.phi = phi
            # self.psis = [[psi1], [psi2], [psi3]]
            # self.plot_effector_inverse()
            # self.plot_legs(pose=1)
            # plt.pause(1e-9)


        # --- Comppute the difference between the initial value and the all the individual values tested ---
        delta = [a-b*np.ones_like(a) for a, b in [[psis1, psi10], [psis2, psi20], [
            psis3, psi30], [betas1, betas10], [betas2, betas20], [betas3, betas30]]]
        delta = np.array(delta)

        # compute the norm 
        dX_s = (thetas1-theta10)**2



        # fig, axs = plt.subplots(3, 2, figsize=(10, 10))
        # axs = axs.flatten()
        thetas1c = thetas1 -np.ones_like(thetas1)*theta10
        # axs[0].plot(thetas1c, delta[0])
        # axs[0].axvline()
        # axs[0].axhline()
        # axs[1].plot(thetas1c,  delta[1])
        # axs[1].axvline()
        # axs[1].axhline()
        # axs[2].plot(thetas1c,  delta[2])
        # axs[2].axvline()
        # axs[2].axhline()
        # axs[3].plot(thetas1c, delta[3])
        # axs[3].axvline()
        # axs[3].axhline()
        # axs[4].plot(thetas1c,  delta[4])
        # axs[4].axvline()
        # axs[4].axhline()
        # axs[5].plot(thetas1c,  delta[5])
        # axs[5].axvline()
        # axs[5].axhline()
        # axs[0].set_xlabel("beta1")
        # axs[1].set_xlabel("beta2")
        # axs[2].set_xlabel("beta3")
        # axs[3].set_xlabel("psi1")
        # axs[4].set_xlabel("psi2")
        # axs[5].set_xlabel("psi3")
        # fig.suptitle("Angles evolution")
        # fig.tight_layout()
        # plt.show()

        # Define the stiffness in each leg
        K = np.array([
            # leg1 all joints actives and we evaluate from 0
            self.stiffness[0][1], self.stiffness[0][2],
            # leg2 joint 0 is blocked by the piezo
            self.stiffness[1][1], self.stiffness[1][2],
            self.stiffness[2][1], self.stiffness[2][2]  # leg3 same as for leg2
        ])
        # Aply the formula F = somme( K_i*delta_theta_i^2 )
        keq = [K[i]*delta[i]**2 /dX_s for i in range(len(delta))]
        keq = np.sum(keq, axis=0)
        # Convert from N/rad to N/deg
        # keq = keq * (2*np.pi/360)
        return keq


    def equivalent_force(self, thetas1: np.ndarray, theta2=None, theta3=None):
        """This function compute the force required to push the mecanism from leg1 (index0)

        Args:
            thetas (np.ndarray): [array of the angle variation]

        Returns:
            [np.array]: [array of the force required at each point]
        """
        # Shortcut if user input
        if theta2 is None:
            theta2 = self.thetas[1][0]
        else :
            theta2 = theta2 + self.thetas[1][0]

        if theta3 is None:
            theta3 = self.thetas[2][0]
        else :
            theta3 = theta3 + self.thetas[2][0]

        # --- Compute the initial value ---
        # get the middle angle for leg 1
        theta10 = thetas1[int(len(thetas1)/2)]
        self.thetas = [[theta10], [theta2], [theta3]]
        # Compute the default angle to get the variatio5n from it
        x0, y0, phi0, psi10, psi20, psi30 = self.direct_kinematics()

        # --- First let's create the vector containing the angles of the legs ---
        psis1 = np.zeros_like(thetas1)
        psis2 = np.zeros_like(thetas1)
        psis3 = np.zeros_like(thetas1)
        # Introducing betas (angle between leg2 and the platform)
        # with beta = -theta - psi + alpha
        betas1 = np.zeros_like(thetas1)
        betas2 = np.zeros_like(thetas1)
        betas3 = np.zeros_like(thetas1)
        # ! Le pi rajoute est parce que l'angle est defini dans le sens inverse (du centre vers l'extremite) et ici on veut de l'extremite vers de le centre
        betas10 = -(theta10+psi10)+self.EFFECTOR_ANGLES[0]+phi0+np.pi
        betas20 = -(theta2+psi20)+self.EFFECTOR_ANGLES[1]+phi0+np.pi
        betas30 = -(theta3+psi30)+self.EFFECTOR_ANGLES[2]+phi0+np.pi

        dx, dy = np.zeros_like(thetas1), np.zeros_like(thetas1) 

        # --- Compute for each single thetas1 angle value the other joints values ---
        for i, th in tqdm(enumerate(thetas1)):
            self.thetas[0][0] = th
            x, y, phi, psi1, psi2, psi3 = self.direct_kinematics()
            psis1[i] = psi1
            psis2[i] = psi2
            psis3[i] = psi3
            betas1[i] = -(th+psi1)+(self.EFFECTOR_ANGLES[0]+phi)+np.pi
            betas2[i] = -(theta2+psi2)+(self.EFFECTOR_ANGLES[1]+phi)+np.pi
            betas3[i] = -(theta3+psi3)+(self.EFFECTOR_ANGLES[2]+phi)+np.pi
            dx[i] = x-x0
            dy[i] = y-y0

            self.beta1 = betas1[i]
            self.beta2 = betas2[i]
            self.beta3 = betas3[i]
            self.x = x
            self.y = y
            self.phi = phi
            self.psis = [[psi1], [psi2], [psi3]]
            # self.plot_effector_inverse()
            # self.plot_legs(pose=1)
            # plt.pause(1e-9)


        # --- Comppute the difference between the initial value and the all the individual values tested ---
        delta = [a-b*np.ones_like(a) for a, b in [[psis1, psi10], [psis2, psi20], [
            psis3, psi30], [betas1, betas10], [betas2, betas20], [betas3, betas30]]]
        delta = np.array(delta)

        # compute the norm 
        dX_s = (thetas1-theta10)**2



        # fig, axs = plt.subplots(3, 2, figsize=(10, 10))
        # axs = axs.flatten()
        # axs[0].plot(thetas1, betas1-betas10)
        # axs[1].plot(thetas1, betas2-betas20)
        # axs[2].plot(thetas1, betas3-betas30)
        # axs[3].plot(thetas1, psis1-psi10)
        # axs[4].plot(thetas1, psis2-psi20)
        # axs[5].plot(thetas1, psis3-psi30)
        # axs[0].set_xlabel("beta1")
        # axs[1].set_xlabel("beta2")
        # axs[2].set_xlabel("beta3")
        # axs[3].set_xlabel("psi1")
        # axs[4].set_xlabel("psi2")
        # axs[5].set_xlabel("psi3")
        # fig.suptitle("Angles evolution")
        # fig.tight_layout()
        # plt.show()

        # Define the stiffness in each leg
        K = np.array([
            # leg1 all joints actives and we evaluate from 0
            self.stiffness[0][1], self.stiffness[0][2],
            # leg2 joint 0 is blocked by the piezo
            self.stiffness[1][1], self.stiffness[1][2],
            self.stiffness[2][1], self.stiffness[2][2]  # leg3 same as for leg2
        ])
        # Aply the formula F = somme( K_i*delta_theta_i^2 ) = Nm/rad
        feq = [K[i]*delta[i]**2 /np.sqrt(dX_s) for i in range(len(delta))]
        feq = np.sum(feq, axis=0)
        
        # Bras de levier
        d = 5e-3
        feq = feq / d
        # # Convert from Nm/rad to Nm/deg
        # feq = feq * (2*np.pi/360)
        return feq

    def on_click(self, event):
        # global hl1p1, hl2p1, hl3p1, hl2p1, hl2p2, hl3p2, triangle, x0, y0, phi0, BASE_RADIUS, BASE_ANGLES
        # fig = event.canvas.figure
        # ax = fig.axes[0]
        self.x = event.xdata
        self.y = event.ydata
        # ax.cla()
        self.ax.axis('equal')
        self.ax.set_xlim(-350, 350)
        self.ax.set_ylim(-350, 350)
        # update the display
        # plot_base(fig, ax, BASE_RADIUS, BASE_ANGLES)
        triangle = self.plot_effector_inverse()
        # self.ax.add_patch(triangle)
        try:
            self.thetas, self.psis = self.invert_geometric_model()
        except ValueError as e:
            return -1

        plot_legs = self.plot_legs()
        # figure
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        return 0

    def scroll(self, event):
        if event.button == 'up':
            self.phi += 2*pi/180
        else:
            self.phi -= 2*pi/180
        self.on_click(event)

    def show(self):
        plt.connect('button_press_event', self.on_click)
        plt.connect('scroll_event', self.scroll)
        plt.show()


if __name__ == '__main__':
    fig1, ax1 = plt.subplots()
    handles = []
    for (theta2, theta3) in [(0,0), (5*np.pi/180, 0), (5*np.pi/180, 5*np.pi/180), (-2*np.pi/180, 0), (-1.1*np.pi/180,-1.1*np.pi/180), (-2*np.pi/180, 5*np.pi/180)] :
    # for (theta2, theta3) in [(0,0), (5*np.pi/180, 0), (5*np.pi/180, 5*np.pi/180)] :
        rrr = Robot()
        theta10 = rrr.thetas[0][0]
        print(f'{theta2=}',f"{theta3=}")
        # rrr.plot_legs(pose=1)
        plt.ion()

        # For forces
        thetas1 = np.linspace(theta10-0.087, theta10+0.087, 50)
        forces = rrr.equivalent_force(thetas1,theta2=theta2, theta3=theta3)
        
        handle, = ax1.plot((thetas1-np.ones_like(thetas1)*theta10)*5e-3, forces,label = f'$\\theta_2 = {round(theta2/np.pi*180,2)}^\circ \\quad \\theta_3={round(theta3/np.pi*180,2)}^\circ$')
        handles.append(handle)
        # For stiffness
        # thetas1 = np.linspace(theta10-0.087, theta10+0.087, 50)
        # forces = rrr.equivalent_stiffness(thetas1,theta2=theta2, theta3=theta3)
        # ax1.plot((thetas1-np.ones_like(thetas1)*theta10), forces,label = f'$\\theta_2 = {round(theta2/np.pi*180,2)}^\\ocirc, \\theta_3={round(theta3/np.pi*180,2)}^\\ocirc$')
    
    leg1 = ax1.legend(title=r"\underline{Configuration testees}",handles = handles,loc='center right')
    ax1.add_artist(leg1)
    leg1.set_draggable(state=True)
    
    handles= []
    Fp = 700
    Fpm = 3000
    Cp = 90e-6
    black = ax1.axhline(3000,color='k',ls='--', label="Piezo Simple")
    handles.append(black)
    ax1.axhline(Fp,color='k',ls='--')
    ax1.axvline(Cp/2,color='k',ls='--')
    ax1.axvline(-Cp/2,color='k',ls='--')
    R = 1.3
    Fam = Fpm / R 
    Fa = Fp / R
    Ca = Cp * R
    red=ax1.axhline(Fa,color='r',ls='--', label ="Avec amplificateur" )
    handles.append(red)
    ax1.axhline(Fam,color='r',ls='--')
    ax1.axvline(Ca/2,color='r',ls='--')
    ax1.axvline(-Ca/2,color='r',ls='--')

    leg = ax1.legend(title=r"\underline{Limites Course-Forces (poussage et traction) Piezo}",handles=handles,loc='upper right')
    leg.set_draggable(state=True)
    # leg.set_draggable(True)
    # For forces
    ax1.grid()
    # ax1.set_xlabel("Course [m]")
    # ax1.set_ylabel("Feq [N]")
    ax1.set_xlabel("Course [mm]")
    ax1.set_ylabel("Force [N]")

    # For stiff 
    # todo

    tikz_save('force_recap.tex', axis_width='\\linewidth', axis_height='.2\\paperheight')
    plt.ioff()
    plt.show()
